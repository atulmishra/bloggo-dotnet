# .NET

> Bookmarks and annotations of online resources pertaining to .NET

- [Matt Warren: *Resources for Learning about .NET Internals*](http://mattwarren.org/2018/01/22/Resources-for-Learning-about-.NET-Internals/)
- [ASP .NET boilerplate](https://github.com/aspnetboilerplate/aspnetboilerplate)
- [The Book of the Runtime](https://www.hanselman.com/blog/TheBookOfTheRuntimeTheInternalsOfTheNETRuntimeThatYouWontFindInTheDocumentation.aspx)
- [Easy Way to Create a C# Expression from a String with Roslyn](https://www.strathweb.com/2018/01/easy-way-to-create-a-c-lambda-expression-from-a-string-with-roslyn/)
- [`System.Linq.Dynamic.Core` (.NET Core)](https://github.com/StefH/System.Linq.Dynamic.Core)
- [Lesser known features of C# 7](https://www.youtube.com/watch?v=CDLvAFljIPo) - features for faster and safer low-level development
- [On .NET YouTube channel](https://www.youtube.com/channel/UCvtT19MZW8dq5Wwfu6B0oxw)
  - [ ] Go through more videos and annotate them
- [`dotnet serve` tool](https://github.com/natemcmaster/dotnet-serve)

## [`async`/`await` best practices](https://www.youtube.com/watch?v=My2gAv5Vrkk) by Brandon Minnick

- *Azure Mobile App Services* - login, push
  - [ ] Check this out
- Xamarin utilizes MVVM (I suspect exactly the same best practices as in WPF)
- *Xamarin University*
  - [ ] Check this out, is it free?
- `IAsyncStateMachine` is the backing interface for the state machine generated from an `async` method
- The state machine is a `class` and method locals are turned into class fields and a `MoveNext` method
- The `MoveNext` method shares a name with the `MoveNext` method from `IEnumerable`
- The `MoveNext` method has a huge `switch` in it for all the states and it has a program counter variable that gets increased with each `await`
- Anonymous methods function the same way - method in C# but the generated IL has an encapsulating class with a certain interface implementation
- The `MoveNext` method wraps the `switch` in a `try`/`catch` block which then fill's the `Task`'s `Exception` property ("rethrows" if `await`ed)
- Bad practice example, in a construtor (can't have `async`/`await`): `Task.Run(async () => await Do())`
  - This is fire and forget and the exception from it cannot be handled except for the `UnobservedTaskException` handler
  - Also no guarantee on when that's finished and if we don't keep a reference to the task to `await` it to make sure but rely on it's result, that's a race condition
- Brandod showcases usages with `ICommand`s which is a less interesting as the WPF days and I am not likely to pick up Xamarin
  - [ ] Find out if `ICommand` is used in UWP MVVM
- The `ICommand` implementation showcases another bad practice: `.Wait`ing `Task`s
  - This negates the benefits of `async`/`await` as it will block the main thread while the `async` method executes (so the main thread cannot render, …)
  - One answer is to `await` it since it *is* a `Task`, an `IAwaitable`, so we can `await` it
  - [ ] Check if `IAwaitable` is the correct name or if I got it wrong
  - `.GetAwaiter().GetResult()` would unwrap an exception throws in the `Task` as opposed to just `.Wait` which would if I do have to run something synchronously
- `ConfigureAwait(false)` is handy in libraries and it means we don't have to use another thread and can reuse the current thread (bad for the UI thread)
  - That way the rest of the `async` method after the `await` call continues on the same thread the `await`ed `Task` ran on saving us a context switch
- The obvious tip for simple `async` then `return await` methods is to just return the `Task` and let the method not be `async` (save us a class for the state machine)
  - Watch out thought because in that case an exception thrown in the `Task` won't be caught in a `try`/`catch` block, the caller will receive a failed `Task` instead
  - Same goes for `Task`s dependent on something from a `using` block - we need to keep that around so `await` that `Task`
- [ ] Learn more about `ValueTask` for when in one branch I have to `await` but in another I have a value handy
  - Did Brandon explain that incorrectly? That sounds more like `Task.FromResult` whereas `ValueTask` is a `struct` which is its whole deal
